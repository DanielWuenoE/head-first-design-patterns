@echo off

:inicio
SET ops = 0
cls
echo.
echo              DuckSimulator
echo ____________________________________________
echo.
::echo 1 Ejecutar WeatherStationHeatIndex
echo 2 Ejecutar WeatherStation
echo 3 Compilar WeatherStation
echo 4 Limpiar proyecto
echo 5 Salir
echo.
echo.____________________________________________
echo.

SET/p "ops=Selección>":

if "%ops%" == "0" goto inicio
::if "%ops%" == "1" goto op1
if "%ops%" == "2" goto op2
if "%ops%" == "3" goto op3
if "%ops%" == "4" goto op4
if "%ops%" == "5" goto op5

::Seleccione una opción valida.
echo. "%ops%" , no es una opción valida.
echo.
pause
echo.
goto:inicio

echo
pause
echo
goto: inicio

:op1 
      @cls
      echo.                                
      color 0F
      echo.                 WeatherStationHeatIndex
      echo._______________________________________________ 
      echo.
	  @echo on
          	java WeatherStationHeatIndex
	  @echo off
      echo.
      pause
      color 07
      goto:inicio
      
:op2
      @cls
      echo.         
      color 0F
      echo.                 WeatherStation
      echo._______________________________________________
      echo.
	  @echo on
          	java WeatherStation
	  @echo off
      echo.
      pause  
      color 07
      goto:inicio      

:op3
      @cls
      echo.   
      color 0F
      echo.                Compilando ...
      echo._______________________________________________
      echo.
	  @echo on
          	javac *.java
	  @echo off
      pause      
      color 07
      goto:inicio   
      
:op4
      @cls
      echo.
      color 0F
      echo.               Limpiar Proyecto
	  echo._______________________________________________
      echo.
	  @echo on
          del *.class
	  @echo off
      echo.
      pause
      color 07
      goto:inicio

:op5
      @cls&exit